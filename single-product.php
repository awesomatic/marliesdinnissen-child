<?php
/**
 * The template for displaying single WooCommerce posts
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */



get_header('shop'); ?>

<?php
	/**
	 * woocommerce_before_main_content hook.
	 *
	 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
	 * @hooked woocommerce_breadcrumb - 20
	 */
	//do_action( 'woocommerce_before_main_content' );
?>

	<div id="primary" class="site-content">

		<?php inti_hook_content_before(); ?>

		<div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

			<?php //inti_hook_grid_open(); ?>

			<div class="grid-container">
				<div class="grid-x grid-margin-x align-center">
					<div class="large-10 large-centered cell">

						<?php inti_hook_inner_content_before(); ?>

						<?php // start the loop
						while ( have_posts() ) : the_post(); ?>

						<?php inti_hook_post_before(); ?>

						<?php wc_get_template_part( 'content', 'single-product' ); ?>

						<?php inti_hook_post_after(); ?>

						<?php endwhile; // end of the loop ?>

						<?php inti_hook_inner_content_after(); ?>

					</div>
				</div>
			</div>

			<?php //inti_hook_grid_close(); ?>

		</div><!-- #content -->

		<?php inti_hook_content_after(); ?>

	</div><!-- #primary -->

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
	//	do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
	//	do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer('shop'); ?>
