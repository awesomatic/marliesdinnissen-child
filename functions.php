<?php
/**
 * Inti Foundation Basic Starter Functions
 *
 * @package Inti
 * @author Waqa Studios
 * @since 1.0.0
 * @copyright Copyright (c) 2015, Waqa Studios
 * @license GNU General Public License v2 or later (http://www.gnu.org/licenses/gpl-2.0.html)
 */


/**
 * Child Theme Setup
 * Modify this function to deactivate features in the parent theme
 *
 * See functions.php in Inti Foundation
 * Remove the comment slashes (//) next to the functions
 *
 * Remove or add array elements in add_theme_support() as needed
 */

add_action('after_setup_theme', 'childtheme_override_setup', 11);
function childtheme_override_setup() {

	/**
	 * Inti features
	 */
	add_theme_support(
		'inti-menus',
		array('dropdown-menu', 'off-canvas-menu', 'footer-menu')
	);

	add_theme_support(
		'inti-sidebars',
		array('reveal', 'primary', 'frontpage', 'footer', 'front-page-footer')
	);

	add_theme_support(
		'inti-layouts',
		array('1c', '2c-l', '2c-r', '1c-thin')
	);

	add_theme_support(
		'inti-post-types',
		array('slides')
	);

	add_theme_support(
		'inti-page-templates',
		array('front-page')
	);

	add_theme_support('inti-customizer');

	add_theme_support('inti-theme-options');

	add_theme_support('inti-backgrounds');

	add_theme_support('inti-fonts');

	add_theme_support('inti-breadcrumbs');

	add_theme_support('inti-pagination');

	add_theme_support('inti-post-header-meta');

	add_theme_support('inti-shortcodes');

	add_theme_support('inti-custom-login');

	/**
	 * WordPress features
	 */
	add_theme_support('menus');

	// different post formats for tumblog style posting
	add_theme_support(
		'post-formats',
		array('aside', 'gallery','link', 'image', 'quote', 'status', 'video', 'audio', 'chat')
	);

	/**
	 * 3rd Party Supprt
	 */
	add_theme_support( 'woocommerce' );

	add_theme_support('post-thumbnails');

	add_image_size('thumb-300', 300, 250, true);
	add_image_size('thumb-200', 200, 150, true);


	// RSS feed links to header.php for posts and comments.
	add_theme_support('automatic-feed-links');

	// editor stylesheet for TinyMCE
	//add_editor_style('/library/src/scss/editor.css');

	// load parent translations
	load_theme_textdomain( 'inti' , get_template_directory() . '/languages');

	// load child theme translations
	load_child_theme_textdomain( 'inti-child' , get_stylesheet_directory() . '/languages');

	/**
	 * Load framework files from child theme's framework directory
	 *
	 * locate_template() will first check the child theme for a file in that location,
	 * and if not found, will search the parent theme. Override parent theme files by giving
	 * the child theme versions the same name, set a unique name or add a prefix to load
	 * them in addition to parent theme files.
	 */
	// require_once locate_template('/framework/content/child-content-header.php');
	// require_once locate_template('/framework/content/child-content-footer.php');


}

/**
 * Add your own custom functions below
 */



 function blockgrid_gallery( $output, $atts, $instance ) {
	$atts = shortcode_atts( array(
		'order'   => 'ASC',
		'orderby' => 'menu_order ID',
		'id'      => get_the_ID(),
		'columns' => 3,
		'size'    => 'thumbnail',
		'include' => '',
		'exclude' => '',
		), $atts );

	if ( ! empty( $atts['include'] ) ) {
		$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( ! empty( $atts['exclude'] ) ) {
		$attachments = get_children( array( 'post_parent' => intval( $atts[ 'id' ] ), 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	} else {
		$attachments = get_children( array( 'post_parent' => intval( $atts[ 'id' ] ), 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	}

	if ( empty( $attachments ) )
		return '';

	$output = '<div class="grid-x grid-padding-x small-up-2 medium-up-' . intval( $atts[ 'columns' ] ) . '" >';

	foreach ( $attachments as $id => $attachment ) {
		$img        = wp_get_attachment_image_url( $id, $atts[ 'size' ] );
		$img_srcset = wp_get_attachment_image_srcset( $id, $atts[ 'size' ] );
		$img_full   = wp_get_attachment_image_url( $id, 'full' );

		$caption = ( ! $attachment->post_excerpt ) ? '' : ' data-caption="' . esc_attr( $attachment->post_excerpt ) . '" ';

		$output .= '<div class="cell">'
			. '<a href="' . esc_url( $img_full ) . '">'
			. '<img src="' . esc_url( $img ) . '" ' . $caption . ' class="th" alt="' . esc_attr( $attachment->title ) . '"  srcset="' . esc_attr( $img_srcset ) . '" sizes="(max-width: 50em) 87vw, 680px" />'
			. '</a></div>';
	}

	$output .= '</div>';

	return $output;
}
add_filter( 'post_gallery', 'blockgrid_gallery', 10, 3 );


/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');


add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

add_action('woocommerce_before_single_product', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_single_product', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<div id="primary" class="site-content">	<div id="content" role="main" class="">';
}

function my_theme_wrapper_end() {
    echo '</div></div>';
}


// add_action('woocommerce_before_shop_loop', 'framework_woocommerce_category_filter', 10);
//
// function framework_woocommerce_category_filter() {
//
// 	$product_categories = get_terms( 'product_cat', $args );
//
//
// 	$args = array(
// 	    'taxonomy'   => "product_cat",
// 	    'number'     => $number,
// 	    'orderby'    => $orderby,
// 	    'order'      => $order,
// 	    'hide_empty' => $hide_empty,
// 	    'include'    => $ids
// 	);
//
// 	$product_categories = get_terms($args);
//
//
// 		foreach( $product_categories as $cat ) { echo $cat->name;	}
//
//  }

https://www.themelocation.com/how-to-add-pages-featured-image-to-wocommerces-shop-page/

//add_action('woocommerce_before_shop_loop', 'wp176545_add_feature_image');

function wp176545_add_feature_image() {

echo get_the_post_thumbnail_url( get_option( 'woocommerce_shop_page_id' ) );

}

//

add_filter( 'woocommerce_show_page_title', '__return_false' );


// Remove the sorting dropdown from Woocommerce
//remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 );

// Remove the result count from WooCommerce
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );


// AWESOMATE PRODUCT CATEGORY FILTER
// @url https://developer.wordpress.org/reference/functions/get_terms/

add_action('woocommerce_before_shop_loop', 'framework_tax_filter', 20);
add_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 30);


function framework_tax_filter() {

	$shop_link = get_permalink( wc_get_page_id( 'shop' ) );

	$args = array(
		'hide_empty=0' );



$terms = get_terms( 'product_cat', $args );

$class_all = ( is_shop( $term->name ) ) ? 'active' : ''; // assign this class if we're on the same category page as $term->name


if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {

    $count = count( $terms );
    $i = 0;
    $term_list = '<div class="framework-filter cell large-9"><ul class="menu expanded text-center"><li class="' . $class_all . '"><a href="'. $shop_link .'">Alle items</a></li>';
    foreach ( $terms as $term ) {
$class = ( is_product_category( $term->name ) ) ? 'active' : ''; // assign this class if we're on the same category page as $term->name

        $i++;
        $term_list .= '<li class="' . $class . '"><a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name . '</a></li>';
        if ( $count != $i ) {
            $term_list .= ''; // seperator
        }
        else {
            $term_list .= '</ul></div>';
        }
    }

		echo '<div class="grid-x grid-margin-x">';
    echo $term_list;
		framework_woocommerce_widgets();
		echo '</div>';
}

}


// AWESOMATE WOOCOMMERCE WIDGETS

//add_action('woocommerce_before_shop_loop', 'framework_woocommerce_widgets', 40);

function framework_woocommerce_widgets() { ?>
<div class="cell large-3">
	<ul class="accordion" data-accordion data-allow-all-closed="true">
  <li class="accordion-item" data-accordion-item>

    <!-- Accordion tab title -->
    <a href="#" class="accordion-title">Filter op eigenschappen</a>

    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
      <p><?php dynamic_sidebar('sidebar-shop-filters'); ?></p>
    </div>
  </li>
</ul>

</div>

	 <?php }


// SWAP PRICE AND ADD TO CART BUTTON

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

add_action( 'woocommerce_after_shop_loop_item', 'framework_swappy', 10);

function framework_swappy() { ?>

	<div class="buttonn">
	 <span class="label-up">
		 <?php

		 if ( ! function_exists( 'woocommerce_template_loop_price' ) ) {

			 	/**
				* Get the product price for the loop.
				*/
			 function woocommerce_template_loop_price() {
					 wc_get_template( 'loop/price.php' );
			 }
		 }

		 // The args.
 		$args = array();

 		// NOTICE! Understand what this does before running.
 		$result = woocommerce_template_loop_price($args);

 	 		?>

	 </span>


	 <span class="label-up">

		<?php

		if ( !function_exists( 'woocommerce_template_loop_add_to_cart' ) ) {
		    require_once '/includes/wc-template-functions.php';
		}

		// The args.
		$args = array();

		// NOTICE! Understand what this does before running.
		$result = woocommerce_template_loop_add_to_cart($args);

		?>

	 </span>
 </div>

<?php }

function framework_remove_product_page_sku( $enabled ) {
    if ( ! is_admin() && is_product() ) {
        return false;
    }

    return $enabled;
}
add_filter( 'wc_product_sku_enabled', 'framework_remove_product_page_sku' );

/**********************************
*
* Move Price under title on WooCommerce Single Product Page
*
*
* Following hook use with woocommerce_single_product_summary action and their calling priority
*
* @hooked woocommerce_template_single_title – 5
* @hooked woocommerce_template_single_price – 10
* @hooked woocommerce_template_single_excerpt – 20
* @hooked woocommerce_template_single_add_to_cart – 30
* @hooked woocommerce_template_single_meta – 40
* @hooked woocommerce_template_single_sharing – 50
*
************************************/
// To Move price underneath title use code
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
// remove action which show Price on their default location
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25);
// add action with Priority just more than the woocommerce_template_single_title

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
// remove action which show Price on their default location
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10);
// add action with Priority just more than the woocommerce_template_single_title


/**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    //unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    //unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}


/**
 * Reorder product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

	$tabs['reviews']['priority'] = 15;			// Reviews first
	$tabs['description']['priority'] = 10;			// Description second
	$tabs['additional_information']['priority'] = 5;	// Additional information third

	return $tabs;
}


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 35 );

add_filter('woocommerce_product_description_heading', false);
add_filter('woocommerce_product_additional_information_heading', false);


// Gallery stuff

//Do something if a specific array value exists within a post

// https://codex.wordpress.org/Function_Reference/wp_get_post_terms

// https://wordpress.stackexchange.com/questions/252429/getting-terms-to-have-space-between-them


add_action( 'pre_get_posts', function ( $query ) {
  if ( $query->is_post_type_archive( 'gallery' ) && $query->is_main_query() && ! is_admin() ) {
    $query->set( 'posts_per_page', 99 );
  }
} );


//add_filter( 'woocommerce_show_variation_price', '__return_true' );


function add_theme_caps() {
    $role = get_role( 'shop_manager' );
    $role->add_cap( 'edit_theme_options' );
    //remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
    //remove_submenu_page( 'customize.php', 'customize.php' ); // hide the widgets submenu;
}
add_action( 'admin_init', 'add_theme_caps');


//The Side Menu
add_action( 'admin_menu', 'remove_default_post_type' );

function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}

//The +New Post in Admin Bar
add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );

function remove_default_post_type_menu_bar( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'new-post' );
}

//The Quick Draft Dashboard Widget
add_action( 'wp_dashboard_setup', 'remove_draft_widget', 999 );

function remove_draft_widget(){
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}


// Maintenance

function wpr_maintenance_mode() {
    if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {
        wp_die('<h1>Er wordt gewerkt aan een nieuwe website</h1>

<p>Tot die tijd kun je terecht op <a href="http://marliesdinnissen.wordpress.com">marliesdinnissen.wordpress.com</a></p>

<p></p>', 'Marlies Dinnissen Sieraden');
    }
}
add_action('get_header', 'wpr_maintenance_mode');


// Google Search Console

// Add scripts to wp_head()
function child_theme_head_script() { ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131728154-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131728154-1');
</script>

<?php }
add_action( 'wp_head', 'child_theme_head_script', -1000 );
