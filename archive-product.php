<?php
/**
 * The WooCommerce archive index
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */

get_header('shop'); ?>

<div class="hero-section" style="background: url('<?php echo wp176545_add_feature_image();?>') 50% 50% no-repeat;">

	<div class="hero-section-text">

		<h1><?php	_e('Shop', 'inti'); ?></h1>

	</div>

</div>

	<div id="primary" class="site-content">

		<?php inti_hook_content_before(); ?>

		<div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

			<?php inti_hook_grid_open(); ?>

				<?php inti_hook_inner_content_before(); ?>

        <?php // get the WooCommerce loop

				woocommerce_content(); ?>

				<?php inti_hook_inner_content_after(); ?>

			<?php inti_hook_grid_close(); ?>

		</div><!-- #content -->

		<?php inti_hook_content_after(); ?>

	</div><!-- #primary -->


<?php get_footer('shop'); ?>
