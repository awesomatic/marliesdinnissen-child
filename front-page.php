<?php
/**
 * The main template for a static front page
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */

get_header(); ?>

	<div id="primary" class="site-content">


		<!-- FULL WIDTH SECTION: SLIDER -->

		<!-- <section id="slider" class="owl-carousel owl-theme"> -->
			<?php //get_template_part('loops/loop', 'slides'); ?>
		<!-- </section> -->

		<div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
		  <div class="orbit-wrapper">
		    <div class="orbit-controls">
		      <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/src/img/left-arrow-yellow.png" width="25" /></button>
		      <button class="orbit-next"><span class="show-for-sr">Next Slide</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/library/src/img/right-arrow-yellow.png" width="25" /></button>
		    </div>
				<ul class="orbit-container">

		<?php get_template_part('template-parts/orbit', 'slider'); ?>

	</ul>

	</div>
</div>

			<?php inti_hook_content_before(); ?>

				<div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

					<?php inti_hook_grid_open(); ?>

						<?php inti_hook_inner_content_before(); ?>

							<section id="social-follow" class="front-page-social">
								<?php echo inti_get_off_canvas_social_links(); ?>
							</section>


							<!-- SECTION: FEATURES -->

							<section id="features">

								<div class="grid-x grid-margin-x align-center">
									<div class="cell large-10">

										<div class="grid-x grid-margin-x align-middle">

											<div class="cell medium-6 large-6 front_page_content_1">
												<?php the_field('front_page_content_1'); ?>
											</div>
											<div class="cell medium-6 large-6 front_page_content_1">
												<img src="<?php the_field('front_page_featured_img_1'); ?>" alt="Een creatief cadeau">
											</div>

											<div class="cell medium-6 large-6 front_page_content_1">
												<img src="<?php the_field('front_page_featured_img_2'); ?>" alt="Volg een workshop">
											</div>
											<div class="cell medium-6 large-6 front_page_content_1">
												<?php the_field('front_page_content_2'); ?>
											</div>

										</div>

									</div>
								</div>

							</section>


							<hr class="style12">


							<!-- SECTION: RECENT PRODUCTS -->

							<section id="recent-products">

									<h1>Eigen ontwerpen</h1>

									<?php echo do_shortcode('[products limit="8" columns="4" orderby="popularity"]'); ?>

								<p class="text-center"><a class="button secondary medium" href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">Bekijk alle sieraden <i class="fas fa-arrow-right" style="margin-left:8px;"></i></a></p>
							</section>

							<hr class="style12">


							<!-- SECTION: ABOUT -->

							<div class="grid-x grid-margin-x align-center">
								<div class="cell large-10">

							<?php /* Restore original Post Data */
							wp_reset_query(); ?>

							<section id="about">
								<div class="grid-x grid-margin-x align-middle">
									<div class="cell large-6">
										<?php the_field('front_page_content_3'); ?>
									</div>
									<div class="cell large-6">
										<img src="<?php the_field('front_page_featured_img_3'); ?>" alt="Marlies Dinnissen">
									</div>
								</div>
							</section>


							<!-- SECTION: GALLERY -->

							<section id="gallery-home">
								<div class="grid-x grid-margin-x align-left align-middle">
									<div class="cell small-12 medium-6">

										<?php $args = array('post_type'=> 'gallery', 'post_status' => 'publish', 'posts_per_page' => '5'); ?>

									    <div class="masonry-css">
									    <?php
									    wp_reset_query();
									    $gallery_loop = new WP_Query($args);
									    while($gallery_loop->have_posts()) : $gallery_loop->the_post();
									            if(has_post_thumbnail()) {  ?>

																<div class="masonry-css-item column filterDiv">

																	<article id="post-<?php the_ID(); ?>">
																		<div class="entry-body">

																			<?php inti_hook_post_header_before(); ?>

																			<header class="entry-header">
																				<?php// inti_hook_post_header(); ?>
																			</header>

																			<?php inti_hook_post_header_after(); ?>

																			<div class="container">
																				<img src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('full'); } ?>" alt="Avatar" class="image">
																			  <a href="<?php the_permalink(); ?>">
																					<div class="overlay">
																					<div>
																							<div class="gallery-text text-center">
																								<p style="font-size: 18px;">Bekijk <span class="hide-for-small-only"> </span><i class="fas fa-chevron-circle-right"></i></p>
																							</div>
																					</div>
																			  </div>
																				</a>
																			</div>

																				<footer class="entry-footer">

																					<?php inti_hook_post_footer(); ?>

																				</footer>

																		</div><!-- .entry-body -->
																	</article><!-- #post -->
																</div>
									        <?php }
									        elseif($thumbnail = get_post_meta($post->ID, 'image', true)) { echo 12323; ?>

									           <div class="masonry-css-item">
									           	<img src="<?php echo $thumbnail; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
									           </div>

									        <?php } endwhile; wp_reset_query();
 ?>
												</div>

									</div>


										<!-- SECTION: ABOUT/CONTACT -->

										<div class="cell small-12 medium-6">
											<?php
											/**
											 * Get the page loop
											 * Content of the page that is set as the front-page is
											 * displayed as a kind of header to the rest of
											 * what is displayed later displayed...
											 */
											 get_template_part('loops/loop', 'page'); ?>
										</div>
									</div>


							</section>

						</div>
					</div>

				<?php inti_hook_inner_content_after(); ?>

			<?php inti_hook_grid_close(); ?>

		</div>

		<?php inti_hook_content_after(); ?>

		<!-- FULL WIDTH SECTION: REVIEWS -->

		<section id="testimonials">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<?php get_template_part('loops/loop', 'review'); ?>
				</div>
			</div>
		</section>

		<!-- FULL WIDTH SECTION: INSTAGRAM FEED -->

		<section id="instagram-feed">
			<div class="grid-container">
				<div class="grid-x">
  				<div class="cell">
						<?php dynamic_sidebar('sidebar-front-page-footer'); ?>
					</div>
				</div>
			</div>
		</section>

	</div>

<?php get_footer(); ?>
