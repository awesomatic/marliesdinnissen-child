<?php
/**
 * The main template for WooCommerce pages
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */


get_header(); ?>

<div class="hero-section" style="background: url('<?php echo wpse207895_featured_image();?>') 50% no-repeat;">

	<div class="hero-section-text">

		<h1><?php echo get_the_title(); ?></h1>

	</div>

</div>

<?php framework_do_page_header_background_img(); ?>

	<div id="primary" class="site-content">

		<?php inti_hook_content_before(); ?>

		<div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

			<?php inti_hook_grid_open(); ?>

				<?php inti_hook_inner_content_before(); ?>

				<?php // get the WooCommerce loop
        woocommerce_content(); ?>

				<?php inti_hook_inner_content_after(); ?>

			<?php inti_hook_grid_close(); ?>

		</div><!-- #content -->

		<?php inti_hook_content_after(); ?>

	</div><!-- #primary -->


<?php get_footer(); ?>
