<?php
/**
 * Template Name: Gallery
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <div id="primary" class="site-content">

 	  <?php inti_hook_content_before(); ?>

 		  <div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

 		    <?php inti_hook_grid_open(); ?>

 				  <?php inti_hook_inner_content_before(); ?>

            <?php //$new_posts_filter = new Ajax_Filter_Posts(); echo $new_posts_filter->get_genre_filters('gallery', 'gallery_category'); ?>

      


              <?php $args = array('post_type'=> 'gallery', 'post_status' => 'publish', 'order' => 'DESC'); ?>

      	        <div class="masonry-css">

      	    <?php $gallery_loop = new WP_Query($args);
      	    while($gallery_loop->have_posts()) : $gallery_loop->the_post();
      	            if(has_post_thumbnail()) {  ?>
                      <a href="<?php the_permalink(); ?>" alt="Bekijk deze gallery">
      								  <div class="masonry-css-item">
      	                  <?php the_post_thumbnail('full'); ?>
      								  </div>
                      </a>
      	        <?php }
      	        elseif($thumbnail = get_post_meta($post->ID, 'image', true)) { echo 12323; ?>
      	           <div class="masonry-css-item">
      	                <img src="<?php echo $thumbnail; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
      	            </div>
      	        <?php } endwhile;

                wp_reset_query();

      	        ?>
      				</div>

 				<?php inti_hook_inner_content_after(); ?>

 			<?php inti_hook_grid_close(); ?>

 		</div><!-- #content -->

 		<?php inti_hook_content_after(); ?>

 	</div><!-- #primary -->


 <?php get_footer(); ?>
