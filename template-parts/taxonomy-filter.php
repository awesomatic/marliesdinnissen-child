
<!-- Filter: category -->

<!-- https://stackoverflow.com/questions/46323872/how-to-filter-multiple-elements-items -->

<section id="tax-filter-category">

  <?php
  $args = array( 'hide_empty=0' );

  $terms = get_terms( 'gallery-category', $args );
  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
      $count = count( $terms );
      $i = 0;
      $term_list = '<ul class="menu simple" id="myBtnContainer"><li><button onclick="filterSelection(' . "'all'" . ')" class="btn active">Alle items</button></li>';
      foreach ( $terms as $term ) {
          $i++;
          $term_list .= '<li><button onclick="filterSelection(' . "'$term->slug'" . ')" class="btn" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name . '</button></li>';
      }
      echo $term_list;
      echo '</ul>';
  } ?>

</section>


<!-- Filter tags -->

<section id="tax-filter-tag">

  <?php
  $args = array( 'hide_empty=0' );

  $terms = get_terms( 'gallery-tag', $args );
  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
      $count = count( $terms );
      $i = 0;
      $term_list = '<ul class="menu simple" id="categoryBoxesTags">
';
      foreach ( $terms as $term ) {
          $i++;
          $term_list .= '<li><input checked type="checkbox" onclick="filterSelectionTag();" name="tag" value="' . "$term->slug" . '"> ' . "$term->name" . '<li>';
      }
      echo $term_list;
      echo "</ul>";

  } ?>

</section>


<!-- Filter: combined -->

<section id="tax-filter-combined">

  <?php
  // $args = array( 'hide_empty=0' );
  //
  // $terms = get_terms( array('gallery-tag', 'gallery-category'), $args );
  // if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
  //     $count = count( $terms );
  //     $i = 0;
  //     $term_list = '<ul class="menu simple" id="categoryBoxesTags">';
  //     foreach ( $terms as $term ) {
  //         $i++;
  //       //	$term_list .= '<input type="checkbox" data-category="' . "$term->slug" . '" name="category" checked>' . "$term->name" . '';
  //         $term_list .= '<li><input type="checkbox" onclick="filterSelectionTag();" name="tag" value="' . "$term->slug" . '"> ' . "$term->name" . '<li>';
  //     }
  //     echo $term_list;
  //     echo "</ul>";
  //
  // } ?>

</section>
