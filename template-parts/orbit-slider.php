<?php

$args = array( 'post_type' => 'slide', 'posts_per_page' => 3 );
$loop = new WP_Query( $args );

  while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

      <li class="is-active orbit-slide" style="color: #fff; height:50vh; background: url('<?php echo $thumb['0'];?>') center center no-repeat; background-size: cover;">
        <div class="grid-container">
        <div class="grid-x grid-padding-x align-middle" style="height:52vh; padding: 25px;">
          <div class="cell">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
        <!-- <figure class="orbit-figure">
          <figcaption class="orbit-caption text-center"><a style="color: #fff;" href="<?php the_permalink(); ?>"> Bekijk <?php the_title(); ?></a></figcaption>
        </figure> -->
      </li>

<?php endwhile; ?>

<?php	wp_reset_query(); ?>

  <!-- <nav class="orbit-bullets">
    <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
    <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
    <button data-slide="2"><span class="show-for-sr">Third slide details.</span></button>
    <button data-slide="3"><span class="show-for-sr">Fourth slide details.</span></button>
  </nav> -->
