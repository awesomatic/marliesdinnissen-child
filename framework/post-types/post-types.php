<?php
/**
 * Post Types
 * Custom post types should be added to child themes, this parent theme
 * has no custom post types by default, but if it did they would go here
 *
 * @package Inti
 * @author Stuart Starrs
 * @since 1.0.1
 * @license GNU General Public License v2 or later (http://www.gnu.org/licenses/gpl-2.0.html)
 */

require_once locate_template('/framework/post-types/reviews.php');




if(!function_exists('slide_post_type_init')){
	function slide_post_type_init() {
		$labels = array(
			'name' => _x('Slides', 'post type general name', 'inti'),
			'singular_name' => _x('Slide', 'post type singular name', 'inti'),
			'add_new' => __('Nieuwe slide', 'Slide', 'inti'),
			'add_new_item' => __('Nieuwe slide', 'inti'),
			'edit_item' => __('Edit Slide', 'inti'),
			'new_item' => __('Nieuwe Slide', 'inti'),
			'view_item' => __('Bekijk Slide', 'inti'),
			'search_items' => __('Zoek slides', 'inti'),
			'not_found' =>  __('Geen slides gevonden', 'inti'),
			'not_found_in_trash' => __('Geen slides gevonden in de prullenbak', 'inti'),
			'parent_item_colon' => '',
			'menu_name' => _x('Slides', '', 'inti')
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'rewrite'   => true,
		'has_archive' => false,
		'query_var' => true,
		'capability_type' => 'page',
		'hierarchical' => true,
		'show_in_nav_menus' => false,
		'menu_position' => 35,
		'menu_icon' => 'dashicons-format-image',
		'supports' => array(
			'title',
			'thumbnail',
			'editor',
			'page-attributes'
			)
		);
		register_post_type('slide',$args);
	}
}
add_action('init', 'slide_post_type_init');








if(!function_exists('gallery_post_type_init')){
	function gallery_post_type_init() {
		$labels = array(
			'name' => _x('Gallery', 'post type general name', 'inti'),
			'singular_name' => _x('Gallery', 'post type singular name', 'inti'),
			'add_new' => __('Nieuwe gallery', 'item', 'inti'),
			'add_new_item' => __('Add New item', 'inti'),
			'edit_item' => __('Edit Item', 'inti'),
			'new_item' => __('New Item', 'inti'),
			'view_item' => __('View Item', 'inti'),
			'search_items' => __('Search Items', 'inti'),
			'not_found' =>  __('No Items found', 'inti'),
			'not_found_in_trash' => __('No Item found in Trash', 'inti'),
			'parent_item_colon' => '',
			'menu_name' => _x('Gallery', '', 'inti')
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'rewrite'   => true,
		'has_archive' => true,
		'query_var' => true,
		'capability_type' => 'page',
		'hierarchical' => true,
		'show_in_nav_menus' => true,
		'menu_position' => 35,
		'menu_icon' => 'dashicons-layout',
		'supports' => array(
			'title',
			'thumbnail',
			'editor',
			'page-attributes'
			)
		);
		register_post_type('Gallery',$args);
	}
}
add_action('init', 'gallery_post_type_init');





// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_gallery_taxonomies', 0 );

// create two taxonomies, genres and Tags for the post type "gallery"
function create_gallery_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Categorieën', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Categorie', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Categories', 'textdomain' ),
		'all_items'         => __( 'All Categories', 'textdomain' ),
		'parent_item'       => __( 'Parent Category', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Category:', 'textdomain' ),
		'edit_item'         => __( 'Edit Category', 'textdomain' ),
		'update_item'       => __( 'Update Category', 'textdomain' ),
		'add_new_item'      => __( 'Add New Category', 'textdomain' ),
		'new_item_name'     => __( 'New Category Name', 'textdomain' ),
		'menu_name'         => __( 'Categorieën', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'has_archive' 			=> true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'gallery-category' ),
	);

	register_taxonomy( 'gallery-category', array( 'gallery' ), $args );

	//Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Tags', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Tag', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Tags', 'textdomain' ),
		'popular_items'              => __( 'Popular Tags', 'textdomain' ),
		'all_items'                  => __( 'All Tags', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Tag', 'textdomain' ),
		'update_item'                => __( 'Update Tag', 'textdomain' ),
		'add_new_item'               => __( 'Add New Tag', 'textdomain' ),
		'new_item_name'              => __( 'New Tag Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Tags with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove Tags', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Tags', 'textdomain' ),
		'not_found'                  => __( 'No Tags found.', 'textdomain' ),
		'menu_name'                  => __( 'Tags', 'textdomain' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'tag' ),
	);

	register_taxonomy( 'gallery-tag', 'gallery', $args );
}
