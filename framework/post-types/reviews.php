<?php

if(!function_exists('reviews_post_type_init')){
	function review_post_type_init() {
		$labels = array(
			'name' => _x('Reviews', 'post type general name', 'inti'),
			'singular_name' => _x('review', 'post type singular name', 'inti'),
			'add_new' => __('Nieuwe review', 'review', 'inti'),
			'add_new_item' => __('Nieuwe review toevoegen', 'inti'),
			'edit_item' => __('Review bewerken', 'inti'),
			'new_item' => __('Nieuwe review', 'inti'),
			'view_item' => __('Bekijk review', 'inti'),
			'search_items' => __('Zoek reviews', 'inti'),
			'not_found' =>  __('Geen reviews gevonden', 'inti'),
			'not_found_in_trash' => __('Geen reviews gevonden in de prullenbak', 'inti'),
			'parent_item_colon' => '',
			'menu_name' => _x('Reviews', '', 'inti')
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'rewrite'   => true,
		'has_archive' => false,
		'query_var' => true,
		'capability_type' => 'page',
		'hierarchical' => true,
		'show_in_nav_menus' => false,
		'menu_position' => 45,
		'menu_icon' => 'dashicons-star-filled',
		'supports' => array(
			'title',
			'thumbnail',
			'editor',
			'page-attributes'
			)
		);
		register_post_type('review',$args);
	}
}
add_action('init', 'review_post_type_init');
