<?php
/**
 * Scripts
 * WordPress will add these scripts to the theme
 *
 * @package Inti
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/wp_register_script
 * @link http://codex.wordpress.org/Function_Reference/wp_enqueue_script
 * @see wp_register_script
 * @see wp_enqueue_script
 * @license GNU General Public License v2 or later (http://www.gnu.org/licenses/gpl-2.0.html)
 */

add_action('wp_enqueue_scripts', 'inti_register_scripts', 1);
add_action('wp_enqueue_scripts', 'inti_enqueue_scripts');

function inti_register_scripts() {
	wp_register_script('toastr-js', get_stylesheet_directory_uri() . '/library/src/js/toastr.min.js', array(), filemtime(get_stylesheet_directory() . '/library/src/js/toastr.min.js'), true);
	wp_register_script('jquery-cookie-js', get_stylesheet_directory_uri() . '/library/src/js/jquery.cookie.js', array(), filemtime(get_stylesheet_directory() . '/library/src/js/jquery.cookie.js'), true);
	wp_register_script('owl-carousel-js', get_stylesheet_directory_uri() . '/node_modules/owlcarousel2/dist/owl.carousel.js', array(), filemtime(get_stylesheet_directory() . '/node_modules/owlcarousel2/dist/owl.carousel.js'), true);
	wp_register_script('what-input-js', 'https://cdnjs.cloudflare.com/ajax/libs/what-input/4.2.0/what-input.min.js' , '', '', true);
	wp_register_script('foundation-js', 'https://cdn.jsdelivr.net/npm/foundation-sites@6.4.3/dist/js/foundation.min.js' , '', '', true);
	wp_register_script('app-js', get_stylesheet_directory_uri() . '/library/src/js/app.js', array(), filemtime(get_stylesheet_directory() . '/library/src/js/app.js'), true);
	wp_register_script('tax-filter-js', get_stylesheet_directory_uri() . '/library/src/js/tax-filter.js', array(), filemtime(get_stylesheet_directory() . '/library/src/js/tax-filter.js'), true);
}

function inti_enqueue_scripts() {
	if ( !is_admin() ) {

		//wp_enqueue_script('jquery'); // built-in
		wp_enqueue_script('toastr-js');
		wp_enqueue_script('jquery-cookie-js');
		//wp_enqueue_script('owl-carousel-js');
		wp_enqueue_script('what-input-js');
		wp_enqueue_script('foundation-js');
		wp_enqueue_script('app-js');

			if (is_post_type_archive('gallery')) {
				wp_enqueue_script('tax-filter-js');
			}

			// if (is_front_page()) {
			// 	wp_enqueue_script('tax-filter-js');
			// }

		// comment reply script for threaded comments
		if ( is_singular() && comments_open() && get_option('thread_comments') ) {
			wp_enqueue_script('comment-reply');
		}
	}
}
