<?php

$args = array('post_type' => 'review', 'orderby' => 'type', 'posts_per_page' => '3');
$the_query = new WP_Query( $args );

// The testimonials loop
if ( $the_query->have_posts() ) {
  while ( $the_query->have_posts() ) {
    $the_query->the_post(); ?>

        <div class="cell small-12 medium-4 large-4">
          <div class="testimonial-block-vertical">
            <div class="testimonial-block-vertical-quote">
              <p><?php the_content(); ?></p>
            </div>
            <div class="testimonial-block-vertical-person">
              <div>
                <p class="testimonial-block-vertical-name"><?php the_title(); ?></p>
              </div>
              <ul class="menu" style="margin-left:10px;">
                <li><i class="fas fa-star"></i>
                <li><i class="fas fa-star"></i>
                <li><i class="fas fa-star"></i>
                <li><i class="fas fa-star"></i>
                <li><i class="fas fa-star"></i>
              </ul>
            </div>
          </div>
        </div>

  <?php  } ?>


    <?php /* Restore original Post Data */
    wp_reset_query();

} else {

    echo "No posts found";

}
?>
