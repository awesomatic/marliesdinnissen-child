  <?php

  $args = array( 'post_type' => 'slide', 'posts_per_page' => 3 );
  $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post(); ?>

      <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

        <div class="framework-slide" style="background: url('<?php echo $thumb['0'];?>') 50% no-repeat;">
          <h2 style="font-size:50px;" class="caption"><?php the_title(); ?></h2>
          <p>><?php the_content(); ?></p>
        </div>

  <?php endwhile; ?>

<?php	wp_reset_query(); ?>
