<?php
/**
* Template Name: Contact
*
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */


get_header(); ?>

<div class="hero-section" style="background: url('<?php echo wpse207895_featured_image();?>') 50% no-repeat;">

	<div class="hero-section-text">

		<h1><?php echo get_the_title(); ?></h1>

	</div>

</div>

	<div id="primary" class="site-content">

		<?php inti_hook_content_before(); ?>

		<div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

			<?php inti_hook_grid_open(); ?>

				<?php inti_hook_inner_content_before(); ?>

				<?php // get the main loop
				get_template_part('loops/loop', 'page'); ?>

				<section class="acf-contact">

					<div class="grid-x">
						<div class="cell small-1">
							<p><i class="far fa-address-card"></i></p>
							<p><i class="fas fa-mobile-alt"></i></p>
							<p><i class="far fa-envelope-open"></i></p>
						</div>
						<div class="cell small-9">

							<!-- ACF -->
							<p><?php the_field('adress'); ?></p>
							<p><?php the_field('mobile'); ?></p>
							<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
						</div>
					</div>

				</section>

				<?php inti_hook_inner_content_after(); ?>

			<?php inti_hook_grid_close(); ?>

      <div class="grid-x" data-equalizer>

          <div class="cell small-12 medium-6 large-6">
            <div class="responsive-embed" data-equalizer-watch>
							<?php the_field('google_maps'); ?>
						</div>
          </div>

        <div class="cell small-12 medium-6 large-6">
          <div class="framework-contact-container" data-equalizer-watch>
						<?php the_field('contactform'); ?>
					</div>
        </div>

      </div>

		</div><!-- #content -->

		<div class="front-page-social">
			<div class="grid-container fluid">
				<div class="grid-x grid-margin-x align-center">
					<div class="small-12 cell">
						<?php echo inti_get_off_canvas_social_links(); ?>
						<br>
					</div><!-- .cell -->

				</div><!-- .grid-x -->
			</div>
		</div><!-- .footer-social -->

		<?php inti_hook_content_after(); ?>

	</div><!-- #primary -->


<?php get_footer(); ?>
